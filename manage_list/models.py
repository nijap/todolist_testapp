from django.db import models

# Create your models here.
class List(models.Model):
    name         = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return self.name
    
class Todo_note(models.Model):
    list         = models.ForeignKey(List)
    title        = models.CharField(max_length=100)
    completed    = models.BooleanField(default = True)
    created_date = models.DateTimeField(auto_now_add=True)
    
    def __unicode__(self):
        return self.title
    

    
  
