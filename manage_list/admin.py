from django.contrib     import admin
from manage_list.models import *
from reversion.admin    import VersionAdmin

# Register your models here.
class ListAdmin(VersionAdmin):
    history_latest_first = True
    
class NotesAdmin(VersionAdmin):
    history_latest_first = True
    
admin.site.register(List, ListAdmin)
admin.site.register(Todo_note, NotesAdmin)
