/*global angular */

/**
 * Services that persists and retrieves todos from localStorage or a backend API
 * if available.
 *
 * They both follow the same API, returning promises for all changes to the
 * model.
 */
angular.module('todomvc')
    .factory('todoStorage', function ($http, $injector) {
        'use strict';

        // Detect if an API backend is present. If so, return the API module, else
        // hand off the localStorage adapter
        return $http.get('/api')
            .then(function () {
                return $injector.get('api');
            }
            , function () {
                return $injector.get('localStorage');
            });
    })

    .factory('api', function ($resource) {
        'use strict';

        var store = {
            todos: [],
            list: [],
    
            api: $resource('/api/todos/:id', null,
                {
                    update: {method: 'PUT'}
                }
            ),
            
            list_api: $resource('/api/list/:id', null,
                {
                    update: {method: 'PUT'}
                }
            ),
            listnote_api: $resource('/api/list_note/:id', null,
                {
                    get: {method:'GET',isArray:false}
                }
            ),
            deletecompleted_api: $resource('/api/delete_completed/:id', null,
                {
                    get: {method:'GET',isArray:false}
                }
            ),
            clearCompleted: function (list_id) {
                var originalTodos = store.todos.slice(0);

                var incompleteTodos = store.todos.filter(function (todo) {
                    return !todo.completed;
                });

                angular.copy(incompleteTodos, store.todos);
                if(list_id==undefined)
                        list_id = '0'
                return store.deletecompleted_api.query({id: list_id }, function () {
                }, 
                   function error() {
                    angular.copy(originalTodos, store.todos);
                });
            },

            delete: function (todo) {
                var originalTodos = store.todos.slice(0);

                store.todos.splice(store.todos.indexOf(todo), 1);
                return store.api.delete({id: todo.id},
                    function () {
                    }, function error() {
                        angular.copy(originalTodos, store.todos);
                    });
            },

            get: function () {
                return store.api.query(function (resp) {
                    angular.copy(resp, store.todos);
                });
            },
            get_list: function () {
                return store.list_api.query(function (resp) {
                    
                    angular.copy(resp, store.list);
                });
            },

            insert: function (todo) {
                var originalTodos = store.todos.slice(0);

                return store.api.save(todo,
                    function success(resp) {
                        todo.id = resp.id;
                        store.todos.push(todo);
                    }, function error() {
                        angular.copy(originalTodos, store.todos);
                    })
                    .$promise;
            },
            insertlist: function (todo) {
                var originalTodos = store.todos.slice(0);

                return store.list_api.save(todo,
                    function success(resp) {
                        todo.id = resp.id;
                        store.list.push(todo);
                    }, function error() {
                        angular.copy(originalTodos, store.todos);
                    })
                    .$promise;
            },

            put: function (todo) {
                return store.api.update({id: todo.id}, todo)
                    .$promise;
            },
            
            selectPerList: function (list_id) {
                return store.listnote_api.query({id: list_id},
                function success(resp) {
                    if(!resp.length)
                    {
                        angular.copy(resp, store.todos);
                    }
                    else{
                        angular.copy(resp, store.todos);
                    }
                }).$promise;
            }
        };

        return store;
    })

    .factory('localStorage', function ($q) {
        'use strict';

        var STORAGE_ID = 'todos-angularjs';

        var store = {
            todos: [],

            _getFromLocalStorage: function () {
                return JSON.parse(localStorage.getItem(STORAGE_ID) || '[]');
            },

            _saveToLocalStorage: function (todos) {
                localStorage.setItem(STORAGE_ID, JSON.stringify(todos));
            },

            clearCompleted: function () {
                var deferred = $q.defer();

                var incompleteTodos = store.todos.filter(function (todo) {
                    return !todo.completed;
                });

                angular.copy(incompleteTodos, store.todos);

                store._saveToLocalStorage(store.todos);
                deferred.resolve(store.todos);

                return deferred.promise;
            },

            delete: function (todo) {
                var deferred = $q.defer();

                store.todos.splice(store.todos.indexOf(todo), 1);

                store._saveToLocalStorage(store.todos);
                deferred.resolve(store.todos);

                return deferred.promise;
            },

            get: function () {
                var deferred = $q.defer();

                angular.copy(store._getFromLocalStorage(), store.todos);
                deferred.resolve(store.todos);

                return deferred.promise;
            },

            insert: function (todo) {
                var deferred = $q.defer();

                store.todos.push(todo);

                store._saveToLocalStorage(store.todos);
                deferred.resolve(store.todos);

                return deferred.promise;
            },

            put: function (todo, index) {
                var deferred = $q.defer();

                store.todos[index] = todo;

                store._saveToLocalStorage(store.todos);
                deferred.resolve(store.todos);

                return deferred.promise;
            }
        };

        return store;
    });
