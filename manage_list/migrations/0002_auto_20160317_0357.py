# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_list', '0001_initial'),
    ]

    operations = [
        migrations.RenameField('List', 'title', 'name'),
    ]
