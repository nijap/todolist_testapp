# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manage_list', '0002_auto_20160317_0357'),
    ]

    operations = [
        migrations.RenameField('todo_note', 'note', 'title'),
    ]
