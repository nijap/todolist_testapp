from django.conf.urls       import include, url
from rest_framework         import routers,serializers, viewsets
from manage_list.models     import *
from django.shortcuts       import HttpResponse
import json

#serializer classes

class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo_note
        fields = ('url','id','list', 'title', 'completed')
        
        
class ListSerializer(serializers.ModelSerializer):
    class Meta:
        model  = List
        fields = ('url', 'id', 'name')
       
#serializer classes end

class ListSet(viewsets.ModelViewSet):
    queryset = List.objects.all()
    serializer_class = ListSerializer
        
class NoteSet(viewsets.ModelViewSet):
    queryset = Todo_note.objects.all()
    serializer_class = NoteSerializer
    
class ListNoteSet(viewsets.ViewSet):
    def retrieve(self,request,pk=None):
        if pk ==None:
            queryset = Todo_note.objects.all()
        else:
            queryset = Todo_note.objects.filter(list__pk=pk)
        if not queryset:
            return HttpResponse({})
        else:
            note_dict = {}
            note_list = []
            for nt in queryset:
                note_dict = {
                'title':nt.title,
                'id':nt.id,
                'list':nt.list.id,
                'completed':nt.completed
                }
                note_list.append(note_dict)
            return HttpResponse(json.dumps(note_list))
        
class DeleteCompletedSet(viewsets.ViewSet):
    def retrieve(self,request,pk=None):
        if pk == '0':
            del_obj = Todo_note.objects.filter(completed = True)
        else:
            del_obj = Todo_note.objects.filter(list__pk=pk, completed = True)
        del_obj.delete()
        return HttpResponse({})


router = routers.DefaultRouter()
router.register(r'list', ListSet)
router.register(r'todos', NoteSet)
router.register(r'list_note', ListNoteSet,base_name='list_note')
router.register(r'delete_completed', DeleteCompletedSet,base_name='delete_completed')

urlpatterns = [
    url(r'^', include(router.urls)),
]