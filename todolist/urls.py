from django.conf.urls import include, url
from django.contrib import admin
from manage_list import views

urlpatterns = [
    url(r'^$', 'manage_list.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^api/', include('rest_api.urls')),
    url(r'^admin/', include(admin.site.urls)),
]
